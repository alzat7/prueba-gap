﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PruebaGAP.Models
{
    public class UserProfile
    {
        public UserProfile()
        {
            InsuredUsers = new HashSet<InsuredUser>();
        }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual ICollection<InsuredUser> InsuredUsers { get; set; }
    }
}
