﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaGAP.Models.Enums
{
    public enum RiskType
    {
        Low,
        Medium,
        MediumHigh,
        High
    }
}
