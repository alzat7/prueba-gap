﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaGAP.Models.Enums
{
    public enum UserStatusType
    {
        Active,
        Inactive
    }
}
