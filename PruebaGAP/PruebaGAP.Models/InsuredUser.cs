﻿using PruebaGAP.Models.Enums;
using System;
using System.Collections.Generic;

namespace PruebaGAP.Models
{
    public class InsuredUser
    {
        public InsuredUser()
        {
            InsurancePolicies = new HashSet<InsurancePolicy>();
        }
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public UserStatusType Status { get; set; }
        public virtual ICollection<InsurancePolicy> InsurancePolicies { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
