﻿using AutoMapper;
using PruebaGAP.Dtos;
using PruebaGAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Profile = AutoMapper.Profile;

namespace PruebGAP.WebApi.Profiles
{
    public class InsurancePolicyProfile : Profile
    {
        public InsurancePolicyProfile()
        {
            CreateMap<InsurancePolicy, InsurancePolicyDto>().ReverseMap();
        }
    }
}
