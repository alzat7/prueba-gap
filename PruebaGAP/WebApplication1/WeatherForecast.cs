using PruebaGAP.Models;
using PruebaGAP.Models.Enums;
using System;

namespace WebApplication1
{
    public class WeatherForecast
    {
        public int Id { get; set; }
        public int InsuranceCoverageId { get; set; }
        public int CoverageTimeId { get; set; }
        public string PolicyName { get; set; }
        public string PolicyDescription { get; set; }
        public RiskType RiskType { get; set; }
    }
}
