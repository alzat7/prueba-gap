﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PruebaGAP.Data;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Dtos;
using PruebaGAP.Models;
using PruebaGAP.Models.Enums;

namespace PruebGAP.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuredUsersController : ControllerBase
    {
        private readonly IBaseRepository<InsuredUser> _userRepository;
        private readonly IBaseRepository<InsurancePolicy> _insuranceRepository;
        private readonly IMapper _mapper;

        public InsuredUsersController(IBaseRepository<InsuredUser> repository, IBaseRepository<InsurancePolicy> insuranceRepository, IMapper mapper)
        {
            _userRepository = repository;
            _insuranceRepository = insuranceRepository;
            _mapper = mapper;
        }

        // GET: api/InsuredUsers
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<InsuredUserDto>>> GetInsuredUsers()
        {
            var result = await _userRepository.FechAllData();
            return _mapper.Map<List<InsuredUserDto>>(result);
        }

        // GET: api/InsuredUsers/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<InsuredUserDto>> GetInsuredUser(int id)
        {
            var insuredUser = await _userRepository.FetchById(id);

            if (insuredUser == null)
            {
                return NotFound();
            }

            return _mapper.Map<InsuredUserDto>(insuredUser);
        }

        // PUT: api/InsuredUsers/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<InsuredUserDto>> PutInsuredUser(int id, InsuredUserDto insuredUserDto)
        {
            if (id != insuredUserDto.Id)
            {
                return BadRequest();
            }

            var insuredUser = _mapper.Map<InsuredUser>(insuredUserDto);
            var result = await _userRepository.Update(insuredUser);

            if (!result)
            {
                return BadRequest();
            }
            return _mapper.Map<InsuredUserDto>(result);
        }

        // POST: api/InsuredUsers
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<InsuredUserDto>> Post(InsuredUserDto insuredUserDto)
        {
            try
            {
                var insuredUser = _mapper.Map<InsuredUser>(insuredUserDto);

                var newUser = await _userRepository.Create(insuredUser);
                if (newUser == null)
                {
                    return BadRequest();
                }

                var newInsuredUserDto = _mapper.Map<InsuredUserDto>(newUser);
                return CreatedAtAction(nameof(Post), new { id = newInsuredUserDto.Id }, newInsuredUserDto);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/InsuredUsers/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteInsuredUser(int id)
        {
            try
            {
                var resultado = await _userRepository.Delete(id);
                if (!resultado)
                {
                    return BadRequest();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<InsurancePolicy>>> GetPoliciesByUserId(int id)
        {
            var user = await _userRepository.FetchById(id);
            return user.InsurancePolicies.ToList();
        }

        public async Task<ActionResult<InsuredUserDto>> AddPolicy(int userId, InsurancePolicyDto insurancePolicyDto)
        {
            var user = await _userRepository.FetchById(userId);
            var insuancePolicy = _mapper.Map<InsurancePolicy>(insurancePolicyDto);
            user.InsurancePolicies.Add(insuancePolicy);

            var newUser = await _userRepository.Update(user);
            var newInsuredUserDto = _mapper.Map<InsuredUserDto>(newUser);
            return CreatedAtAction(nameof(Post), new { id = newInsuredUserDto.Id }, newInsuredUserDto);
        }

        [HttpDelete]
        public async Task<IActionResult> DeletePolicy(int userId, int policyType)
        {
            var user = await _userRepository.FetchById(userId);
            foreach (var policy in user.InsurancePolicies)
            {
                if ((int)policy.PolicyType == policyType)
                {
                    await _insuranceRepository.Delete(policy.Id);
                }
            }

            return NoContent();
        }
    }
}
