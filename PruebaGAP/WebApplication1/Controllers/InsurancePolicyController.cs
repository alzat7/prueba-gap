﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Dtos;
using PruebaGAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebGAP.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InsurancePolicyController : ControllerBase
    {
        private readonly IBaseRepository<InsurancePolicy> _repository;
        private readonly ILogger<InsurancePolicyController> _logger;
        private readonly IMapper _mapper;

        public InsurancePolicyController(IBaseRepository<InsurancePolicy> repository, ILogger<InsurancePolicyController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        // GET: InsurancePolicy
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<InsurancePolicyDto>>> Get()
        {
            try
            {
                var result = await _repository.FechAllData();
                return  _mapper.Map<List<InsurancePolicyDto>>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error fetching policies: ${ex.Message}");
                return BadRequest();
            }
        }

        // GET: InsurancePolicy/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<InsurancePolicyDto>> Get(int id)
        {
            var policy = await _repository.FetchById(id);
            if (policy == null)
            {
                return NotFound();
            }

            return _mapper.Map<InsurancePolicyDto>(policy);
        }

        // POST: InsurancePolcy
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<InsurancePolicyDto>> Post(InsurancePolicyDto insurancePolicyDto)
        {
            try
            {
                var policy = _mapper.Map<InsurancePolicy>(insurancePolicyDto);

                var newPolicy = await _repository.Create(policy);
                if (newPolicy == null)
                {
                    return BadRequest();
                }

                var newPolicyDto = _mapper.Map<InsurancePolicyDto>(newPolicy);
                return CreatedAtAction(nameof(Post), new { id = newPolicyDto.Id }, newPolicyDto);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: InsurancePolicy/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<InsurancePolicyDto>> Put(int id, [FromBody]InsurancePolicyDto insurancePolicyDto)
        {
            if (insurancePolicyDto == null)
            {
                return NotFound();
            }
            var insurancePolicy = _mapper.Map<InsurancePolicy>(insurancePolicyDto);
            var result = await _repository.Update(insurancePolicy);
            if (!result)
            {
                return BadRequest();
            }
            return _mapper.Map<InsurancePolicyDto>(result);
        }

        // DELETE: InsurancePolicy/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var resultado = await _repository.Delete(id);
                if (!resultado)
                {
                    return BadRequest();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
