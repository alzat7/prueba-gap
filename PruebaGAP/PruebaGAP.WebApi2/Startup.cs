using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using PruebaGAP.Data;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Data.Repositories;
using PruebaGAP.Models;
using PruebaGAP.WebApi2.Services;
using System;
using System.Text;

namespace PruebaGAP.WebApi2
{
    public class Startup
    {
        public IConfiguration _configuration { get; }
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration; 
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);

            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<InsuranceDbContext>(options =>
                options.UseSqlServer(_configuration.GetConnectionString("InsuranceDb")));

            services.AddControllers();

            services.AddScoped<IBaseRepository<InsurancePolicy>, InsuranceRepository>();

            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IPasswordHasher<InsuredUser>, PasswordHasher<InsuredUser>>();

            services.AddScoped<IBaseRepository<UserProfile>, ProfileRepository>();

            services.AddSingleton<TokenService>();

            var jwtSettings = _configuration.GetSection("JwtSettings");

            string secretKey = jwtSettings.GetValue<string>("SecretKey");

            int minutesToExpiration = jwtSettings.GetValue<int>("MinutesToExpiration");

            string issuer = jwtSettings.GetValue<string>("Issuer");

            string audience = jwtSettings.GetValue<string>("Audience");

            var key = Encoding.ASCII.GetBytes(secretKey);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => 
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = true,
                    ValidIssuer = issuer,
                    ValidateAudience = true,
                    ValidAudience = audience,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(minutesToExpiration)
                };
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
