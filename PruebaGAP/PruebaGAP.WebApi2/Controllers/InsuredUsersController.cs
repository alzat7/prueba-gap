﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Dtos;
using PruebaGAP.Models;
using PruebaGAP.WebApi2.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebGAP.WebApi2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InsuredUsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IBaseRepository<InsurancePolicy> _insuranceRepository;
        private readonly ILogger<InsuredUsersController> _logger;
        private readonly IMapper _mapper;

        public InsuredUsersController(IUserRepository userRepository, 
            IBaseRepository<InsurancePolicy> insuranceRepository, 
            ILogger<InsuredUsersController> logger,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _insuranceRepository = insuranceRepository;
            _logger = logger;
            _mapper = mapper;
        }

        // GET: insuredUsers
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<InsuredUserDto>>> GetInsuredUsers()
        {
            try
            {
                var result = await _userRepository.FechAllData();
                return _mapper.Map<List<InsuredUserDto>>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error fetching users: ${ex.Message}");
                return BadRequest();
            }        
        }

        // GET: insuredUsers/{id}
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<InsuredUserDto>> GetInsuredUser(int id)
        {
            var insuredUser = await _userRepository.FetchById(id);

            if (insuredUser == null)
            {
                return NotFound();
            }

            return _mapper.Map<InsuredUserDto>(insuredUser);
        }

        // PUT: insuredUsers/{id}
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<InsuredUserDto>> PutInsuredUser(int id, InsuredUserDto insuredUserDto)
        {
            if (id != insuredUserDto.Id)
            {
                return BadRequest();
            }

            var insuredUser = _mapper.Map<InsuredUser>(insuredUserDto);
            var result = await _userRepository.Update(insuredUser);

            if (!result)
            {
                return BadRequest();
            }
            return _mapper.Map<InsuredUserDto>(result);
        }

        // POST: insuredUsers
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<InsuredUserDto>> Post(InsuredUserDto insuredUserDto)
        {
            try
            {
                var insuredUser = _mapper.Map<InsuredUser>(insuredUserDto);

                var newUser = await _userRepository.Create(insuredUser);
                if (newUser == null)
                {
                    return BadRequest();
                }

                var newInsuredUserDto = _mapper.Map<InsuredUserDto>(newUser);
                return CreatedAtAction(nameof(Post), new { id = newInsuredUserDto.Id }, newInsuredUserDto);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error creating user: ${ex.Message}");
                return BadRequest();
            }
        }

        // DELETE: insuredUsers/{id}
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteInsuredUser(int id)
        {
            try
            {
                var resultado = await _userRepository.Delete(id);
                if (!resultado)
                {
                    return BadRequest();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: insuredUsers/policies/{id}
        [HttpGet("policies/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<InsurancePolicy>>> GetPoliciesByUserId(int id)
        {
            try
            {
                var policies = await _insuranceRepository.FechAllData();
                return policies.Where(x => x.UserId == id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error fetching users policies: ${ex.Message}");
                return BadRequest(); ;
            }            
        }

        // PUT: insuredUsers/policies/add/{id}
        [HttpPut("policies/add/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<bool>> AddPolicy(int id, InsurancePolicyDto insurancePolicyDto)
        {
            try
            {
                var validator = new PolicyValidator();
                if (!validator.HiRiskRule(insurancePolicyDto))
                {
                    return BadRequest("el porcentaje de cubrimiento no puede ser superior al 50%");
                }
                var user = await _userRepository.FetchById(id);
                var insuancePolicy = _mapper.Map<InsurancePolicy>(insurancePolicyDto);
                user.InsurancePolicies.Add(insuancePolicy);
                return await _userRepository.Update(user);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error adding users policies: ${ex.Message}");
                return BadRequest();
            }         
        }

        // DELETE: insuredUsers/policies/delete/{id}
        [HttpDelete("policies/delete/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeletePolicy(int id)
        {
            try
            {
                var resultado = await _insuranceRepository.Delete(id);
                if (!resultado)
                {
                    return BadRequest();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
