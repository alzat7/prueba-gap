﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Dtos;
using PruebaGAP.Models;
using PruebaGAP.WebApi2.Services;
using System;
using System.Threading.Tasks;

namespace PruebaGAP.WebApi2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IUserRepository _repository;
        private IMapper _mapper;
        private ILogger<LoginController> _logger;
        private TokenService _tokenService;

        public LoginController(IUserRepository repository, 
            IMapper mapper,
            ILogger<LoginController> logger,
            TokenService tokenService)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
            _tokenService = tokenService;
        }

        //POST: login
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<string>> PostLogin(LoginDto userLogin)
        {
            try
            {
                var userLoginData = _mapper.Map<InsuredUser>(userLogin);
                var result = await _repository.ValidateLogin(userLoginData);

                if (!result.result)
                {
                    return BadRequest("Usuario/Contraseña Inválidos.");
                }

                return _tokenService.GenerarToken(result.insuredUser);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in login: ${ex.Message}");
                return BadRequest();
            }            
        }
    }
}