﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Dtos;
using PruebaGAP.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaGAP.WebApi2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly IBaseRepository<UserProfile> _repository;
        private readonly ILogger<ProfilesController> _logger;
        private readonly IMapper _mapper;

        public ProfilesController(IBaseRepository<UserProfile> repository, 
            ILogger<ProfilesController> logger, 
            IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        // GET: profiles
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<UserProfileDto>>> Get()
        {
            try
            {
                var result = await _repository.FechAllData();
                return _mapper.Map<List<UserProfileDto>>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error fetching profiles: ${ex.Message}");
                return BadRequest();
            }
        }

        // GET: profiles/{id}
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserProfileDto>> Get(int id)
        {
            var profile = await _repository.FetchById(id);

            if (profile == null)
            {
                return NotFound();
            }

            return _mapper.Map<UserProfileDto>(profile);
        }

        // PUT: profiles/{id}
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UserProfileDto>> Put(int id, UserProfileDto profileDto)
        {
            if (id != profileDto.Id)
            {
                return BadRequest();
            }

            var profile = _mapper.Map<UserProfile>(profileDto);
            var result = await _repository.Update(profile);

            if (!result)
            {
                return BadRequest();
            }
            return _mapper.Map<UserProfileDto>(result);
        }

        // POST: profiles
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UserProfileDto>> Post(UserProfileDto profileDto)
        {
            try
            {
                var profile = _mapper.Map<UserProfile>(profileDto);

                var newProfile = await _repository.Create(profile);
                if (newProfile == null)
                {
                    return BadRequest();
                }

                var newProfileDto = _mapper.Map<UserProfileDto>(newProfile);
                return CreatedAtAction(nameof(Post), new { id = newProfileDto.Id }, newProfileDto);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error creating profiles: ${ex.Message}");
                return BadRequest();
            }
        }

        // DELETE: profiles/{id}
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteProfile(int id)
        {
            try
            {
                var resultado = await _repository.Delete(id);
                if (!resultado)
                {
                    return BadRequest();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }  
    }
}
