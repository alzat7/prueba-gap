﻿using AutoMapper;
using PruebaGAP.Dtos;
using PruebaGAP.Models;

namespace PruebGAP.WebApi2.Profiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<InsurancePolicy, InsurancePolicyDto>()
                .ForMember(i => i.InsuredUser, p => p.MapFrom(n => $"{n.InsuredUser.Name} {n.InsuredUser.LastName}"))
                .ReverseMap()
                .ForMember(p => p.InsuredUser, i => i.Ignore());

            CreateMap<InsuredUser, InsuredUserDto>()
                .ForMember(u => u.UserProfile, p => p.MapFrom(n => n.UserProfile.Name))
                .ReverseMap()
                .ForMember(u => u.UserProfile, p => p.Ignore());

            CreateMap<UserProfile, UserProfileDto>().ReverseMap();

            CreateMap<InsuredUser, LoginDto>().ReverseMap();
        }
    }
}
