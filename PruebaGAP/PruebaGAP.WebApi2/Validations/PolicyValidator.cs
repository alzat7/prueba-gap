﻿using PruebaGAP.Dtos;
using PruebaGAP.Models.Enums;

namespace PruebaGAP.WebApi2.Validations
{
    public class PolicyValidator
    {
        private const decimal HiRiskMaxPercent = 0.5m;
        public bool HiRiskRule(InsurancePolicyDto insurancePolicyDto)
        {
            if (insurancePolicyDto.RiskType == RiskType.High)
            {
                return insurancePolicyDto.CoveragePercent <= HiRiskMaxPercent ? true : false;
            }
            return true;
        }
    }
}
