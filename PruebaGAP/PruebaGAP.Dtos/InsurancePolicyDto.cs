﻿using PruebaGAP.Models.Enums;
using System;

namespace PruebaGAP.Dtos
{
    public class InsurancePolicyDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string PolicyName { get; set; }
        public string PolicyDescription { get; set; }
        public RiskType RiskType { get; set; }
        public PolicyType PolicyType { get; set; }
        public decimal CoveragePercent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int CoveragePeriod { get; set; }
        public string InsuredUser { get; set; }
    }
}
