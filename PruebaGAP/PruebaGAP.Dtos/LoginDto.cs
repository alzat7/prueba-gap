﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaGAP.Dtos
{
    public class LoginDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
