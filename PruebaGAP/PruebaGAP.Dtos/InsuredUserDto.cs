﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaGAP.Dtos
{
    public class InsuredUserDto
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserProfile { get; set; }
    }
}
