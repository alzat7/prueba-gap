﻿using PruebaGAP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaGAP.Dtos
{
    public class UserProfileDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
