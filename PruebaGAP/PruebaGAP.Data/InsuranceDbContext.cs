﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PruebaGAP.Data.DbConfigurations;
using PruebaGAP.Models;

namespace PruebaGAP.Data
{
    public partial class InsuranceDbContext : DbContext
    {
        public InsuranceDbContext()
        {
        }

        public InsuranceDbContext(DbContextOptions<InsuranceDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<InsurancePolicy> InsurancePolicies { get; set; }
        public virtual DbSet<InsuredUser> InsuredUsers { get; set; }
        public virtual DbSet<UserProfile> Profiles { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.ApplyConfiguration(new InsurancePolicyConfiguration());

            modelBuilder.ApplyConfiguration(new InsuredUserConfiguration());

            modelBuilder.ApplyConfiguration(new UserProfileConfiguration());
        }
    }
}
