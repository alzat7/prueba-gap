﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaGAP.Data.Repositories
{
    public class ProfileRepository : IBaseRepository<UserProfile>
    {
        private readonly InsuranceDbContext _context;
        private readonly DbSet<UserProfile> _profiles;
        private readonly ILogger<ProfileRepository> _logger;
        public ProfileRepository(InsuranceDbContext context, ILogger<ProfileRepository> logger)
        {
            _context = context;
            _logger = logger;
            _profiles = context.Set<UserProfile>();
        }
        public async Task<UserProfile> Create(UserProfile entity)
        {
            _profiles.Add(entity);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Create error: {ex.Message}");
                throw;
            }
            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _profiles.SingleOrDefaultAsync(u => u.Id == id);
            _profiles.Remove(entity);
            try
            {
                return (await _context.SaveChangesAsync() > 0 ? true : false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete error: {ex.Message}");
                throw;
            }
        }

        public async Task<IEnumerable<UserProfile>> FechAllData()
        {
            return await _profiles.ToListAsync();         
        }

        public async Task<UserProfile> FetchById(int id)
        {
            return await _profiles.SingleOrDefaultAsync(profile => profile.Id == id);
        }

        public async Task<bool> Update(UserProfile entity)
        {
            _profiles.Attach(entity);
            try
            {
                return await _context.SaveChangesAsync() > 0 ? true : false;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Update error: {ex.Message}");
                throw;
            }
        }
    }
}
