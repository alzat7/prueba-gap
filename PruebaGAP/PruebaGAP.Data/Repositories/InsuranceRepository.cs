﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaGAP.Data.Repositories
{
    public class InsuranceRepository : IBaseRepository<InsurancePolicy>
    {
        private readonly InsuranceDbContext _context;
        private readonly ILogger<InsuranceRepository> _logger;
        private readonly DbSet<InsurancePolicy> _insurancePolicies;
        public InsuranceRepository(InsuranceDbContext context, ILogger<InsuranceRepository> logger)
        {
            _context = context;
            _logger = logger;
            _insurancePolicies = context.Set<InsurancePolicy>();
        }
        public async Task<InsurancePolicy> Create(InsurancePolicy entity)
        {
            _insurancePolicies.Add(entity);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Create error: {ex.Message}");
            }
            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _insurancePolicies.SingleOrDefaultAsync(u => u.Id == id);
            _insurancePolicies.Remove(entity);
            try
            {
                return (await _context.SaveChangesAsync() > 0 ? true : false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete error: {ex.Message}");
            }
            return false;
        }

        public async Task<IEnumerable<InsurancePolicy>> FechAllData()
        {
            return await _insurancePolicies.ToListAsync();            
        }

        public async Task<InsurancePolicy> FetchById(int id)
        {
            return await _insurancePolicies.SingleOrDefaultAsync(policy => policy.Id == id);
        }

        public async Task<bool> Update(InsurancePolicy entity)
        {
            _insurancePolicies.Attach(entity);
            try
            {
                return await _context.SaveChangesAsync() > 0 ? true : false;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Update error: {ex.Message}");
                throw;
            }
        }
    }
}
