﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PruebaGAP.Data.Contracts;
using PruebaGAP.Models;
using PruebaGAP.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PasswordVerificationResult = Microsoft.AspNetCore.Identity.PasswordVerificationResult;

namespace PruebaGAP.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly InsuranceDbContext _context;
        private readonly DbSet<InsuredUser> _insuredUsers;
        private readonly IPasswordHasher<InsuredUser> _passwordHasher;
        private readonly ILogger<UserRepository> _logger;

        public UserRepository(InsuranceDbContext context, IPasswordHasher<InsuredUser> passwordHasher, ILogger<UserRepository> logger)
        {
            _context = context;
            _passwordHasher = passwordHasher;
            _logger = logger;
            _insuredUsers = context.Set<InsuredUser>();
        }

        public async Task<InsuredUser> Create(InsuredUser entity)
        {
            entity.Status = UserStatusType.Active;
            entity.Password = _passwordHasher.HashPassword(entity, entity.Password);
            _insuredUsers.Add(entity);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Create error: {ex.Message}");
            }
            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _insuredUsers.SingleOrDefaultAsync(u => u.Id == id);
            _insuredUsers.Remove(entity);
            try
            {
                return (await _context.SaveChangesAsync() > 0 ? true : false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Delete error: {ex.Message}");
            }
            return false;
        }

        public async Task<IEnumerable<InsuredUser>> FechAllData()
        {
            return await _insuredUsers.ToListAsync();
        }

        public async Task<InsuredUser> FetchById(int id)
        {
            return await _insuredUsers.SingleOrDefaultAsync(user => user.Id == id);
        }

        public async Task<bool> Update(InsuredUser entity)
        {
            _insuredUsers.Attach(entity);
            try
            {
                return await _context.SaveChangesAsync() > 0 ? true : false;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Update error: {ex.Message}");
                throw;
            }
        }

        public async Task<(bool result, InsuredUser insuredUser)> ValidateLogin(InsuredUser userLoginData)
        {
            var userDb = await _insuredUsers
                                    .Include(u => u.UserProfile)
                                    .FirstOrDefaultAsync(u => u.Username == userLoginData.Username);
            if (userDb != null)
            {
                try
                {
                    var resultado = _passwordHasher.VerifyHashedPassword(userDb, userDb.Password, userLoginData.Password);
                    return (resultado == PasswordVerificationResult.Success ? true : false, userDb);
                }
                catch (Exception excepcion)
                {
                    _logger.LogError($"Error en {nameof(ValidateLogin)}: " + excepcion.Message);
                }
            }
            return (false, null);
        }
    }
}
