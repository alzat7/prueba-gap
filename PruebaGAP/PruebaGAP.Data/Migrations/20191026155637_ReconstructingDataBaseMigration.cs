﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PruebaGAP.Data.Migrations
{
    public partial class ReconstructingDataBaseMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "insurance");

            migrationBuilder.CreateTable(
                name: "UserProfile",
                schema: "insurance",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfile", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InsuredUser",
                schema: "insurance",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProfileId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 256, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    Username = table.Column<string>(maxLength: 25, nullable: false),
                    Password = table.Column<string>(maxLength: 512, nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuredUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsuredUser_UserProfile_ProfileId",
                        column: x => x.ProfileId,
                        principalSchema: "insurance",
                        principalTable: "UserProfile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InsurancePolicy",
                schema: "insurance",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PolicyName = table.Column<string>(maxLength: 256, nullable: false),
                    PolicyDescription = table.Column<string>(maxLength: 256, nullable: false),
                    RiskType = table.Column<int>(nullable: false),
                    PolicyType = table.Column<int>(nullable: false),
                    CoveragePercent = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    CoveragePeriod = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsurancePolicy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsurancePolicy_InsuredUser_UserId",
                        column: x => x.UserId,
                        principalSchema: "insurance",
                        principalTable: "InsuredUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InsurancePolicy_UserId",
                schema: "insurance",
                table: "InsurancePolicy",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuredUser_ProfileId",
                schema: "insurance",
                table: "InsuredUser",
                column: "ProfileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InsurancePolicy",
                schema: "insurance");

            migrationBuilder.DropTable(
                name: "InsuredUser",
                schema: "insurance");

            migrationBuilder.DropTable(
                name: "UserProfile",
                schema: "insurance");
        }
    }
}
