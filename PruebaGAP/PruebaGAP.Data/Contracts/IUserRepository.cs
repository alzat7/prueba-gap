﻿using PruebaGAP.Models;
using System.Threading.Tasks;

namespace PruebaGAP.Data.Contracts
{
    public interface IUserRepository : IBaseRepository<InsuredUser>
    {
        Task<(bool result, InsuredUser insuredUser)> ValidateLogin(InsuredUser insuredUser);
    }
}
