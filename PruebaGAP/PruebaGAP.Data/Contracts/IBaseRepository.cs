﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaGAP.Data.Contracts
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IEnumerable<T>> FechAllData();
        Task<T> FetchById(int id);
        Task<T> Create(T entity);
        Task<bool> Update(T entity);
        Task<bool> Delete(int id);
    }
}
