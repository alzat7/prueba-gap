﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PruebaGAP.Models;

namespace PruebaGAP.Data.DbConfigurations
{
    public class InsuredUserConfiguration : IEntityTypeConfiguration<InsuredUser>
    {
        public void Configure(EntityTypeBuilder<InsuredUser> entity)
        {
            entity.ToTable("InsuredUser", "insurance");

            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(256);

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);

            entity.Property(e => e.Password)
                .IsRequired()
                .HasMaxLength(512);

            entity.Property(e => e.Username)
                .IsRequired()
                .HasMaxLength(25);

            entity.HasOne(e => e.UserProfile)
                .WithMany(i => i.InsuredUsers)
                .HasForeignKey(j => j.ProfileId);
        }
    }
}
