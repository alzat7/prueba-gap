﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PruebaGAP.Models;

namespace PruebaGAP.Data.DbConfigurations
{
    public class InsurancePolicyConfiguration : IEntityTypeConfiguration<InsurancePolicy>
    {
        public void Configure(EntityTypeBuilder<InsurancePolicy> entity)
        {
            entity.ToTable("InsurancePolicy", "insurance");

            entity.Property(e => e.CoveragePercent).HasColumnType("decimal(18, 2)");

            entity.Property(e => e.PolicyDescription)
                .IsRequired()
                .HasMaxLength(256);

            entity.Property(e => e.PolicyName)
                .IsRequired()
                .HasMaxLength(256);

            entity.HasOne(e => e.InsuredUser)
                 .WithMany(i => i.InsurancePolicies)
                 .HasForeignKey(j => j.UserId);
        }
    }
}
