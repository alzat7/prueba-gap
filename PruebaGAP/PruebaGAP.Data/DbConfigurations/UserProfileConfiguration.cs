﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PruebaGAP.Models;

namespace PruebaGAP.Data.DbConfigurations
{
    public class UserProfileConfiguration : IEntityTypeConfiguration<UserProfile>
    {
        public void Configure(EntityTypeBuilder<UserProfile> entity)
        {
            entity.ToTable("UserProfile", "insurance");

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(256);

        }
    }
}
